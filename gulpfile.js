var gulp = require('gulp'),
    rename = require('gulp-rename'),
    clean = require('gulp-clean'),
    sass = require('gulp-ruby-sass'),
    sourcemaps   = require('gulp-sourcemaps'),
    browserSync  = require('browser-sync'),
    useref = require('gulp-useref'),
    reload       = browserSync.reload;

var src = {
    scss: 'src/assets/stylesheets/*.scss',
    js: 'src/assets/javascripts/*.js',
    html: 'src/*.html'
};

function sassStream () {
    return sass('src/assets/stylesheets/', {sourcemap: true, compass: true})
        .on('error', function (err) {
            console.error('Error!', err.message);
        })
        .pipe(sourcemaps.write('./', {
            includeContent: false,
            sourceRoot: 'src/assets/stylesheets/'
        }));
}

gulp.task('serve', ['default'], function() {

    browserSync({
        server: "./dist"
    });

    gulp.watch(src.scss, ['css']);
    gulp.watch(src.html, ['html']).on('change', reload);
});

gulp.task('css', function() {
    return sassStream()
        .pipe(rename('template_styles.css'))
        .pipe(gulp.dest('dist/'))
        .pipe(reload({stream: true}));
});

gulp.task('js', function() {
    gulp.src('src/assets/javascripts/app.js')
        .pipe(rename('script.js'))
        .pipe(gulp.dest('dist/'));
});

gulp.task('clean', function() {
    gulp.src('dist/')
        .pipe(clean({force: true}));
});

gulp.task('html', function() {
    var assets = useref.assets();

    gulp.src('src/*.html')
        .pipe(assets)
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('./'));
});

gulp.task('default', ['clean', 'css', 'js', 'html']);